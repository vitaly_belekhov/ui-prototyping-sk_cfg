#ifndef SK_DEVICES_FWD
#define SK_DEVICES_FWD

static const uint16_t MP_device_id = 0x10;
static const uint16_t MIG_device_id = 0x11;
static const uint16_t NNK_device_id = 0x12;
static const uint16_t IK_device_id = 0x13;
static const uint16_t MB_base_device_id = 0x80;

#endif // SK_DEVICES_FWD

