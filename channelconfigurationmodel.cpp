#include "channelconfigurationmodel.h"
#include <algorithm>

ChannelsConfigurationModel::ChannelsConfigurationModel(QObject *parent)
    : QAbstractItemModel(parent)
{
}

void ChannelsConfigurationModel::reset(const mode_config_t &mode_config)
{
    mode_config_ = mode_config;
    column_count_ = static_cast<int>(std::max_element(mode_config_.begin(),
                                                      mode_config_.end(),
                                                      [](const channel_config_list_t &a, const channel_config_list_t &b) {
                 return a.size() < b.size();
             })->size());
}

int ChannelsConfigurationModel::rowCount(const QModelIndex &parent) const
{
    if ( parent.isValid() )
        return static_cast<int>(mode_config_[parent.row()].size());
    else
        return static_cast<int>(mode_config_.size());
}

int ChannelsConfigurationModel::columnCount(const QModelIndex &parent) const
{
    if ( parent.isValid() )
        return static_cast<int>(mode_config_[parent.row()].size());
    else
        return 0;
}

int ChannelsConfigurationModel::maxColumnCount() const
{
    return column_count_;
    /*
    return static_cast<int>(std::max_element(mode_config_.begin(),
                                             mode_config_.end(),
                                             [](const channel_config_list_t &a, const channel_config_list_t &b) {
        return a.size() < b.size();
    })->size());
    */
}

QVariant ChannelsConfigurationModel::data(const QModelIndex &index, int role) const
{
    if ( index.isValid() && index.row() < mode_config_.size() && index.column() < mode_config_[index.row()].size() )
    {
        switch ( role ) {
        case NameRole:
            return mode_config_[index.row()][index.column()].name;
        case BitsRole:
            return mode_config_[index.row()][index.column()].bits;
        case ParityRole:
            return mode_config_[index.row()][index.column()].parity;
        default:
            return QVariant();
        }
    }
    else
        return QVariant();
}

QHash<int, QByteArray> ChannelsConfigurationModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[BitsRole] = "bits";
    roles[ParityRole] = "parity";
    return roles;
}

QModelIndex ChannelsConfigurationModel::index(int row, int /*column*/, const QModelIndex &parent) const
{
    if ( parent.isValid() )
        return createIndex(parent.row(), row);
    else
        return createIndex(row, 0);
}

QModelIndex ChannelsConfigurationModel::parent(const QModelIndex &child) const
{
    return createIndex(child.row(), 0);
}
