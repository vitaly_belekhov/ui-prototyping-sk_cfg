#ifndef CHANNELSDATAMODEL_H
#define CHANNELSDATAMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <deque>

class ChannelsDataModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum role_t {
        RoleMP = Qt::UserRole + 1,
        RoleMB,
        RoleCHARGE,
        RoleMIG,
        RoleDEVI,
        RoleGTF,
        Role2NNK,
        RoleMIK
    };

    explicit ChannelsDataModel(QObject *parent = 0);

signals:

public slots:
    void append(const int role, const QString data);
    void clear();

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    QHash<int, std::deque<QString>> channels_data_;
};

#endif // CHANNELSDATAMODEL_H
