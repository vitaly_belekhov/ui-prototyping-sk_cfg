import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQml.Models 2.2
import SK.Toolstring 1.0

ScrollView {
    anchors.fill: parent
    Item {
        property real insets: 6
        implicitHeight: cly.implicitHeight + 2*insets
        implicitWidth: cly.implicitWidth + 2*insets
        ColumnLayout {
            id: cly
            anchors.centerIn: parent
            GroupBox {
                title: qsTr("Параметры гидроканала")
                GridLayout {
                    columns: 2
                    Text {
                        Layout.alignment: Qt.AlignLeft
                        font.pointSize: 10
                        text: qsTr("Кодировка")
                        renderType: Text.NativeRendering
                    }
                    RowLayout {
                        ExclusiveGroup { id: hydroChannelGroup }
                        RadioButton {
                            text: "RLL"
                            exclusiveGroup: hydroChannelGroup
                        }
                        RadioButton {
                            text: "TimeBase"
                            checked: true
                            exclusiveGroup: hydroChannelGroup
                        }
                    }
                    Text {
                        Layout.alignment: Qt.AlignLeft
                        font.pointSize: 10
                        text: qsTr("Ширина импульса (мс)")
                        renderType: Text.NativeRendering
                    }
                    TextField {
                        Layout.alignment: Qt.AlignRight
                        Layout.fillWidth: true
                        font.pointSize: 10
                        inputMethodHints: Qt.ImhDigitsOnly
                        validator: IntValidator {bottom: 50; top: 10000;}
                        text: qsTr("1000")
                    }
                    Text {
                        Layout.alignment: Qt.AlignLeft
                        font.pointSize: 10
                        text: qsTr("Порог по давлению (атм)")
                        renderType: Text.NativeRendering
                    }
                    TextField {
                        Layout.alignment: Qt.AlignRight
                        Layout.fillWidth: true
                        font.pointSize: 10
                        inputMethodHints: Qt.ImhDigitsOnly
                        validator: IntValidator {bottom: 1; top: 500;}
                        text: qsTr("20")
                    }

                }
            }

            ChannelsConfigView {
                model : appCANDriver.noteObject.staticChannelsConfigurationModel
                title: qsTr("Статический замер")
                delegate: ChannelConfigDelegate {}
                minimumCellWidth: 70
            }

            ChannelsConfigView {
                model : appCANDriver.noteObject.dynamicChannelsConfigurationModel
                title: qsTr("Динамический замер")
                delegate: ChannelConfigDelegate {}
                minimumCellWidth: 70
            }
        }
    }
}
