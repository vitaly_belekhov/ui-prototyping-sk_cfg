#include "founddevicesmodel.h"
#include "sk_devices_fwd.h"

static const QHash<int, QString> device_id_name_map = {
    { MP_device_id, "МП" },
    { MIG_device_id, "МИГ" },
    { NNK_device_id, "ННК" },
    { IK_device_id, "ИК" },
    { MB_base_device_id, "МБ" },
};

FoundDevicesModel::FoundDevicesModel(QObject *parent) : QAbstractListModel(parent)
{
    //resetWithTestData();
}

void FoundDevicesModel::resetWithTestData()
{
    beginResetModel();
    this->found_devices_ = {
        { MP_device_id, 21, "МП"},
        { MB_base_device_id, 0, "МБ"},
        { MB_base_device_id + 2, 2, "МБ"},
        { MB_base_device_id + 4, 4, "МБ"},
        { MIG_device_id, 43, "МИГ"},
    };
    endResetModel();
}

void FoundDevicesModel::clear()
{
    beginResetModel();
    this->found_devices_.clear();
    endResetModel();
}

void FoundDevicesModel::rowUp(int row)
{
    beginMoveRows(QModelIndex(), row, row, QModelIndex(), row - 1);
    std::swap(this->found_devices_[row], this->found_devices_[row - 1]);
    endMoveRows();
}

void FoundDevicesModel::rowDown(int row)
{
    beginMoveRows(QModelIndex(), row, row, QModelIndex(), row + 2);
    std::swap(this->found_devices_[row], this->found_devices_[row + 1]);
    endMoveRows();
}

void FoundDevicesModel::append(const uint16_t id, const uint16_t serial)
{
    if ( id >= MB_base_device_id )
        this->append({id, serial, device_id_name_map[MB_base_device_id]});
    else if ( device_id_name_map.contains(id) )
        this->append({id, serial, device_id_name_map[id]});
}

void FoundDevicesModel::append(const found_device_t &item)
{
    beginInsertRows(QModelIndex(), this->rowCount(), this->rowCount());
    this->found_devices_.push_back(item);
    if ( item.id == MP_device_id && this->found_devices_.size() > 1 )
    {
        std::swap(*this->found_devices_.rbegin(), *this->found_devices_.begin());
    }
    endInsertRows();
}

int FoundDevicesModel::rowCount(const QModelIndex &) const
{
    return static_cast<int>(this->found_devices_.size());
}

QVariant FoundDevicesModel::data(const QModelIndex &index, int role) const
{
    if ( index.isValid() && index.row() < this->found_devices_.size() )
    {
        switch ( role ) {
        case IdRole:
            return this->found_devices_[index.row()].id;
        case SerialRole:
            return this->found_devices_[index.row()].serial;
        case NameRole:
            return this->found_devices_[index.row()].name;
        }
    }
    return QVariant();
}

QHash<int, QByteArray> FoundDevicesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[IdRole] = "id";
    roles[SerialRole] = "serial";
    roles[NameRole] = "name";
    return roles;
}
