#ifndef FOUNDDEVICESMODEL_H
#define FOUNDDEVICESMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <cstdint>
#include <vector>

typedef struct
{
    uint16_t id;
    uint16_t serial;
    QString name;
} found_device_t;

typedef std::vector<found_device_t> found_devices_t;

class FoundDevicesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum {
        IdRole = Qt::UserRole + 1,
        SerialRole,
        NameRole
    };

    explicit FoundDevicesModel(QObject *parent = 0);

    Q_INVOKABLE void resetWithTestData();
    Q_INVOKABLE void rowUp(int row);
    Q_INVOKABLE void rowDown(int row);
    Q_INVOKABLE void append(const found_device_t & item);

signals:

public slots:
    void clear();
    void append(const uint16_t id, const uint16_t serial);

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    found_devices_t found_devices_;
};

#endif // FOUNDDEVICESMODEL_H
