import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import SK.Toolstring 1.0

SplitView {
    id: root
    anchors.fill: parent
    orientation: Qt.Horizontal

    Item {
        id: initController
        property var onResultReadyFunction: function(result) {
            console.log("ToolstringEditor default resultReadyFunction called with result ", result)
        }

        Connections {
            target: appCANDriver
            onResultReady: initController.onResultReadyFunction(result);
        }

        states: [
            State {
                name: ""
                PropertyChanges {
                    target: progressBar;
                    value: value == maximumValue ? maximumValue : 0
                    explicit: true
                    restoreEntryValues: false
                }
                PropertyChanges { target: redoAction; enabled: true; restoreEntryValues: false }
                PropertyChanges { target: goNextAction; enabled: true; restoreEntryValues: false }
            },
            State {
                name: "RESET"
                StateChangeScript {
                    script: { appCANDriver.reset(); }
                }
                PropertyChanges {
                    target: progressBar;
                    value: 0
                    maximumValue: 5
                    restoreEntryValues: false
                }
                PropertyChanges { target: redoAction; enabled: false; restoreEntryValues: false }
                PropertyChanges { target: goNextAction; enabled: false; restoreEntryValues: false }
                PropertyChanges {
                    target: initController;
                    onResultReadyFunction: function(result) {
                        progressBar.value = 1;
                        if (!result) {
                            initController.state = "INIT";
                        }
                        else initController.state = "";
                    }
                }
            },
            State {
                name: "INIT"
                StateChangeScript {
                    script: { appCANDriver.initialize(); }
                }
                PropertyChanges {
                    target: initController;
                    onResultReadyFunction: function(result) {
                        progressBar.value = 2;
                        if (!result) {
                            initController.state = "OPEN";
                        }
                        else initController.state = "";
                    }
                }
            },
            State {
                name: "OPEN"
                StateChangeScript {
                    script: { appCANDriver.open(); }
                }
                PropertyChanges {
                    target: initController;
                    onResultReadyFunction: function(result) {
                        progressBar.value = 3;
                        if (!result) {
                            applicationLogModel.logMessage(qsTr("IXXAT"), false, false, qsTr("Готов к работе"));
                            initController.state = "SCAN";
                        }
                        else initController.state = "";
                    }
                }
            },
            State {
                name: "SCAN"
                StateChangeScript {
                    script: { appCANDriver.scan_devices(); }
                }
                PropertyChanges {
                    target: initController;
                    onResultReadyFunction: function(result) {
                        progressBar.value = 4;
                        if (!result) {
                            if ( appCANDriver.noteObject.foundDevicesModel.rowCount() > 0 ) {
                                applicationLogModel.logMessage(qsTr("IXXAT"), false, false, qsTr("Поиск модулей завершен"));
                                initController.state = "CALIB";
                            } else {
                                applicationLogModel.logMessage(qsTr("IXXAT"), false, true, qsTr("Не найдено ни одного модуля"));
                                initController.state = "";
                            }
                        }
                        else initController.state = "";
                    }
                }
            },
            State {
                name: "CALIB"
                StateChangeScript {
                    script: { appCANDriver.read_calibrations(); }
                }
                PropertyChanges {
                    target: initController;
                    onResultReadyFunction: function(result) {
                        progressBar.value = 5;
                        if (!result) {
                            applicationLogModel.logMessage(qsTr("IXXAT"), false, false, qsTr("Чтение калибровок завершено"));
                        }
                        initController.state = "";
                    }
                }
            }
        ]
    }

    function initFunction() {
        initController.state = "RESET";
    }

    states: [
        State {
            name: "VISIBLE"
            when: (root.visible)
            PropertyChanges
            {
                target: redoAction;
                onTriggeredFunction: root.initFunction
            }
            PropertyChanges { target: redoAction; enabled: true }
        }
    ]

    Component.onCompleted: initFunction()

    Item {
        Layout.minimumWidth: 200
        Layout.fillWidth: true
        Layout.margins: 6
        ColumnLayout {
            anchors.fill: parent
            RowLayout {
                Layout.fillWidth: true
                Button {
                    id: toolDownButton
                    iconSource: "arrowdown.png"
                    tooltip: qsTr("Сдвинуть модуль вниз")
                    enabled: foundDevicesTableView.currentRow > 0 && foundDevicesTableView.currentRow < foundDevicesTableView.rowCount - 2
                    onClicked: {
                        foundDevicesTableView.model.rowDown(foundDevicesTableView.currentRow);
                        foundDevicesTableView.selection.clear();
                        foundDevicesTableView.selection.select(foundDevicesTableView.currentRow + 1);
                    }
                }
                Button {
                    id: toolUpButton
                    iconSource: "arrowup.png"
                    tooltip: qsTr("Сдвинуть модуль вверх")
                    enabled: foundDevicesTableView.currentRow > 1 && foundDevicesTableView.currentRow < foundDevicesTableView.rowCount - 1
                    onClicked: {
                        foundDevicesTableView.model.rowUp(foundDevicesTableView.currentRow);
                        foundDevicesTableView.selection.clear();
                        foundDevicesTableView.selection.select(foundDevicesTableView.currentRow - 1);
                    }
                }

            }

            TableView {
                id: foundDevicesTableView
                Layout.fillWidth: true
                model: appCANDriver.noteObject.foundDevicesModel
//                Layout.maximumHeight: height
//                height: flickableItem.height
                TableViewColumn {
                    title: qsTr("Модуль")
                    role: "name"
                    movable: false
                }
                TableViewColumn {
                    title: qsTr("Номер")
                    role: "serial"
                    movable: false
                }
            }

            Text {
                font.pointSize: 10
                text: qsTr("Заряд батарей (часов): 44")
                renderType: Text.NativeRendering
            }

            ProgressBar {
                id: progressBar
            }

            RowLayout {
                Layout.fillHeight: true
            }

            ApplicationLogView {
            }
        }
    }

    Item {
        Layout.minimumWidth: 150
        Layout.margins: 6
        GridView {
            id: graphicalToolstringView
            flow: GridView.TopToBottom
            cellWidth: width
            cellHeight: height/count
            anchors.fill: parent
            model: appCANDriver.noteObject.foundDevicesModel
            delegate: Item {
                width: graphicalToolstringView.cellWidth
                height: graphicalToolstringView.cellHeight
                RowLayout {
                    anchors.fill: parent
                    Image {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        fillMode: Image.PreserveAspectFit
                        source: "tool.svg"
                    }
                }
            }
        }
    }

}
