import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.2
import SK.Toolstring 1.0

ApplicationWindow {
    id: applicationWindow
    visible: true
    visibility: Window.Maximized
    title: qsTr("Конфигуратор телесистемы LWD")

    onMinimumWidthChanged: {
        width = Math.max(minimumWidth, 150, width);
    }

    onMinimumHeightChanged: {
        height = Math.max(minimumHeight, 150, height);
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("Файл")
            MenuItem {
                action: noteNewAction
            }
            MenuItem {
                action: noteOpenAction
            }
            MenuItem {
                action: noteSaveAction
            }
            MenuItem {
                text: qsTr("Выход")
                onTriggered: Qt.quit();
            }
        }
        Menu {
            title: qsTr("Действия")
            MenuItem {
                action: redoAction
            }
            MenuItem {
                action: goNextAction
            }
        }
    }

    Action {
        id: noteNewAction
        text: qsTr("Новая заявка")
        shortcut: StandardKey.New
        iconSource: "filenew.png"
        enabled: true
        onTriggered: {
            applicationLogModel.logMessage(qsTr("Приложение"), false, false, qsTr("Создана новая заявка"));
            while ( tabView.count )
            {
                tabView.removeTab(tabView.count - 1);
            }
            redoAction.reset();
            noteSaveAction.enabled = true;
            goNextAction.reset();
            goNextAction.enabled = true;
            goNextAction.trigger();
        }
    }

    Action {
        id: noteOpenAction
        text: qsTr("Открыть заявку")
        shortcut: StandardKey.Open
        iconSource: "fileopen.png"
        enabled: true
        onTriggered: {
            noteSaveAction.enabled = true;
            goNextAction.enabled = true;
        }
    }

    Action {
        id: noteSaveAction
        text: qsTr("Сохранить заявку")
        shortcut: StandardKey.Save
        iconSource: "filesave.png"
        enabled: false
        onTriggered: console.log("Save action triggered");
    }

    Action {
        id: redoAction
        text: qsTr("Повторить")
        shortcut: StandardKey.Save
        iconSource: "actionredo.png"
        enabled: false
        function reset() { onTriggeredFunction = function() { console.log("Redo action triggered") }; enabled = false; }
        property var onTriggeredFunction: function() { console.log("Redo action triggered") }
        onTriggered: onTriggeredFunction()
    }

    Action {
        id: goNextAction
        text: qsTr("Далее")
        shortcut: StandardKey.Forward
        iconSource: "actionnext.png"
        enabled: false
        function reset() { enabled = false; goNextAction.current_src_idx = 0; }
        property var srcs: ["NoteInfoView.qml", "ToolstringEditor.qml", "ToolstringTest.qml", "ToolstringConfigurator.qml"]
        property var src_names: [qsTr("Заявка"), qsTr("Конфигурация сборки"), qsTr("Тестирование"), qsTr("Параметры")]
        property int current_src_idx: 0
        onTriggered: {
            if ( tabView.currentIndex + 1 < tabView.count )
            {
                tabView.currentIndex++;
                return;
            }
            if ( current_src_idx >= srcs.length )
            {
                return;
            }
            tabView.addTab(src_names[current_src_idx]).setSource(srcs[current_src_idx]);
            tabView.currentIndex = current_src_idx;
            ++current_src_idx;
        }
    }

    toolBar: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                action: noteNewAction
            }
            ToolButton {
                action: noteOpenAction
            }
            ToolButton {
                action: noteSaveAction
            }
            ToolButton {
                action: redoAction
            }
            ToolButton {
                action: goNextAction
            }
            Item {
                Layout.fillWidth: true
            }
        }
    }

    MessageDialog {
        id: errorDialog
    }

    ListModel {
        id: applicationLogModel

        function logMessage(source, is_highlevel, is_error, message) {
            append({"time": new Date().toLocaleString(Qt.locale(), "dd.MM.yy hh:mm:ss"),
                       "source": source,
                       "is_highlevel": is_highlevel,
                       "is_error": is_error,
                       "message": message})
            if ( is_error && is_highlevel ) {
                errorDialog.title = qsTr("Ошибка");
                errorDialog.text = qsTr("Ошибка ") + source;
                errorDialog.informativeText = message;
                errorDialog.visible = true;
            }
        }
    }

    CANDriver {
        id: appCANDriver
    }

    Connections {
        target: appCANDriver
        onErrorOccured: {
            applicationLogModel.logMessage("IXXAT", true, true, errorText);
        }
        onWarningOccured: {
            applicationLogModel.logMessage("IXXAT", false, true, errorText);
        }
    }

    TabView {
        id: tabView
        anchors.fill: parent
        Layout.minimumWidth: 1000
        Layout.minimumHeight: 600
        Tab {
            onStatusChanged: {
                applicationLogModel.logMessage(qsTr("Приложение"), true, false, qsTr("Запущено"));
                noteNewAction.trigger();
            }
        }
    }
}

