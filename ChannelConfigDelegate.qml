import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    Text {
        text: delegateModel ? delegateModel.name : ""
        renderType: Text.NativeRendering
    }
    Text {
        text: delegateModel ? delegateModel.bits : ""
        renderType: Text.NativeRendering
    }
    CheckBox {
        text: qsTr("P")
        checked: delegateModel ? delegateModel.parity : false
    }
}
