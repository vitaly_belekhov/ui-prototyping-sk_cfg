#ifndef NOTEOBJECT_H
#define NOTEOBJECT_H

#include <QObject>
#include <memory>

class FoundDevicesModel;
class ChannelsConfigurationModel;
class ChannelsDataModel;

class NoteObject : public QObject
{
    Q_OBJECT

    std::unique_ptr<FoundDevicesModel> foundDevicesModel_;
    std::unique_ptr<ChannelsDataModel> testChannelsDataModel_;
    std::unique_ptr<ChannelsConfigurationModel> dynamicChannelsConfigurationModel_;
    std::unique_ptr<ChannelsConfigurationModel> staticChannelsConfigurationModel_;

public:
    explicit NoteObject(QObject *parent = 0);

    Q_PROPERTY(FoundDevicesModel *foundDevicesModel READ foundDevicesModel RESET clearFoundDevicesModel CONSTANT)
    Q_PROPERTY(ChannelsConfigurationModel *dynamicChannelsConfigurationModel READ dynamicChannelsConfigurationModel RESET resetDynamicChannelsConfigurationModel CONSTANT)
    Q_PROPERTY(ChannelsConfigurationModel *staticChannelsConfigurationModel READ staticChannelsConfigurationModel RESET resetStaticChannelsConfigurationModel CONSTANT)
    Q_PROPERTY(ChannelsDataModel *testChannelsDataModel READ testChannelsDataModel RESET clearTestChannelsDataModel CONSTANT)

    FoundDevicesModel * foundDevicesModel();
    void clearFoundDevicesModel() const;
    ChannelsDataModel * testChannelsDataModel();
    void clearTestChannelsDataModel() const;
    ChannelsConfigurationModel * dynamicChannelsConfigurationModel();
    void resetDynamicChannelsConfigurationModel() const;
    ChannelsConfigurationModel * staticChannelsConfigurationModel();
    void resetStaticChannelsConfigurationModel() const;

signals:

public slots:
};

#endif // NOTEOBJECT_H
