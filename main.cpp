#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "candriver.h"
#include "channelconfigurationmodel.h"
#include "founddevicesmodel.h"
#include "channelsdatamodel.h"
#include "noteobject.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qRegisterMetaType<uint16_t>("uint16_t");
    QQmlApplicationEngine engine;
    qmlRegisterType<CANDriver>("SK.Toolstring", 1, 0, "CANDriver");
    qmlRegisterType<FoundDevicesModel>("SK.Toolstring", 1, 0, "FoundDevicesModel");
    qmlRegisterType<ChannelsDataModel>("SK.Toolstring", 1, 0, "ChannelsDataModel");
    qmlRegisterType<ChannelsConfigurationModel>("SK.Toolstring", 1, 0, "ChannelsConfigurationModel");
    qmlRegisterType<NoteObject>("SK.Toolstring", 1, 0, "NoteObject");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
