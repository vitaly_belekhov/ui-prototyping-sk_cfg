import QtQuick 2.5
import QtQuick.Layouts 1.2
import QtQml.Models 2.2

Rectangle {
    id: root
    property var model
    property string title
    property Component delegate
    property real minimumCellWidth: 0
    property real minimumCellHeight: 0
    implicitHeight: configGrid.implicitHeight + 2
    implicitWidth: configGrid.implicitWidth + 2
    border.width: 1
    border.color: "#808080"
    color: "red"
    GridLayout {
        id: configGrid
        anchors.centerIn: parent
        columns: parent.model.maxColumnCount()
        columnSpacing: -1
        rowSpacing: -1
        Rectangle {
            property real insets: 6
            implicitHeight: headerTitle.implicitHeight + 2*insets + 2
            implicitWidth: headerTitle.implicitWidth + 2*insets + 2
            Layout.fillWidth: true
            Layout.columnSpan: parent.columns
            border.width: 1
            border.color: "#D5D5D5"
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#FFFFFF" }
                GradientStop { position: 1.0; color: "#F2F2F2" }
            }
            Text {
                id: headerTitle
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: parent.insets
                text: root.title
                renderType: Text.NativeRendering
            }
        }
        Repeater {
            id: configRow
            model: root.model
            delegate: Repeater {
                id: configCell
                property int rowIndex: index
                model: DelegateModel {
                    model: configRow.model
                    rootIndex: modelIndex(index)
                    delegate: Rectangle {
                        property real insets: 6
                        implicitHeight: Math.max(root.minimumCellHeight, configCellItem.implicitHeight + 2*insets)
                        implicitWidth: Math.max(root.minimumCellWidth, configCellItem.implicitWidth + 2*insets)
                        Layout.fillWidth: true
                        Layout.columnSpan: index >= 0 && parent ? (configCell.count == index + 1 ? parent.columns - index : 1) : 1
                        color: rowIndex % 2 ? "#F5F5F5" : "#FFFFFF"
                        Loader {
                            id: configCellItem
                            property var delegateModel: model
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: parent.insets
                            sourceComponent: root.delegate
                        }
                    }
                }
            }
        }
    }
}
