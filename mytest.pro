TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    candriver.cpp \
    channelconfigurationmodel.cpp \
    founddevicesmodel.cpp \
    candriverworker.cpp \
    channelsdatamodel.cpp \
    noteobject.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    candriver.h \
    channelconfigurationmodel.h \
    founddevicesmodel.h \
    candriverworker.h \
    calibrations.h \
    sk_devices_fwd.h \
    channelsdatamodel.h \
    noteobject.h


win32:contains(QT_ARCH, i386) {
     LIBS += -L$$PWD/IXXAT-SDK/lib/ia32/ -lvcisdk
} else {
     LIBS += -L$$PWD/IXXAT-SDK/lib/x64/ -lvcisdk
}

INCLUDEPATH += $$PWD/IXXAT-SDK/inc
DEPENDPATH += $$PWD/IXXAT-SDK/inc

win32:!win32-g++: {
contains(QT_ARCH, i386) {
      PRE_TARGETDEPS += $$PWD/IXXAT-SDK/lib/ia32/vcisdk.lib
} else {
      PRE_TARGETDEPS += $$PWD/IXXAT-SDK/lib/x64/vcisdk.lib
}
}
else:win32-g++: {
contains(QT_ARCH, i386) {
       PRE_TARGETDEPS += $$PWD/IXXAT-SDK/lib/ia32/libvcisdk.a
} else {
       PRE_TARGETDEPS += $$PWD/IXXAT-SDK/lib/x64/libvcisdk.a
}
}
