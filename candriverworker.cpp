#include "candriverworker.h"
#include "sk_devices_fwd.h"
#include "calibrations.h"
#include "channelsdatamodel.h"
#include <QTime>
#include <QTimerEvent>
#include "vcinpl.h"
#include <map>
#include <cassert>
#include <ctime>

using namespace std::placeholders;

CANDriverWorker::CANDriverWorker(QObject *parent)
    : QObject(parent)
    , hChannel_(canChannelClose)
{
}

void CANDriverWorker::initialize()
{
    CHAR errorBuffer[VCI_MAX_ERRSTRLEN];
    HRESULT vciResult;

    if ( (vciResult = vciInitialize()) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }

    emit resultReady(0);
}

void CANDriverWorker::open()
{
    CHAR errorBuffer[VCI_MAX_ERRSTRLEN];
    HRESULT vciResult;

    handler_guard hEnum(vciEnumDeviceClose);
    if ( (vciResult = vciEnumDeviceOpen(hEnum)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }

    VCIDEVICEINFO deviceInfo;
    if ( (vciResult = vciEnumDeviceNext(hEnum, &deviceInfo)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }
    hEnum.reset();

    handler_guard hDevice(vciDeviceClose);
    if ( (vciResult = vciDeviceOpen(deviceInfo.VciObjectId, hDevice)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }

    handler_guard hControl(canControlClose);
    if ( (vciResult = canControlOpen(hDevice, 0, hControl)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }

    this->hChannel_.reset();
    handler_guard hChannel(canChannelClose);
    if ( (vciResult = canChannelOpen(hDevice, 0, TRUE, hChannel)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }

    hDevice.reset();

    if ( (vciResult = canControlInitialize(hControl, CAN_OPMODE_STANDARD, CAN_BT0_125KB, CAN_BT1_125KB)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }
    if ( (vciResult = canControlStart(hControl, TRUE)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }
    hControl.reset();

    if ( (vciResult = canChannelInitialize(hChannel, 2000, 1, 2000, 2000-1)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }

    if ( (vciResult = canChannelActivate(hChannel, TRUE)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }

    this->hChannel_ = hChannel;

    CANMSG can_msg;
    int i = 0;
    for ( ; i < 10; ++i )
    {
        if ( (vciResult = canChannelReadMessage(this->hChannel_, 1, &can_msg)) != VCI_OK )
        {
            vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
            emit errorOccured(errorBuffer);
            emit resultReady(1);
            return;
        }
        if ( can_msg.uMsgInfo.Bits.type != CAN_MSGTYPE_INFO )
            continue;
        if ( can_msg.abData[0] == 1 )
            break;
    }
    if ( i >= 10 )
    {
        emit errorOccured("Не удалось инициализировать IXXAT");
        emit resultReady(1);
        return;
    }

    if ( (vciResult = canChannelReadMessage(this->hChannel_, 1000, &can_msg)) != VCI_E_TIMEOUT )
    {
        emit errorOccured("Посторонний трафик на CAN");
        emit resultReady(1);
        return;
    }

    emit resultReady(0);
}

void CANDriverWorker::reset()
{
    killTimer(this->timer_id_);
    this->hChannel_.reset();
    this->devices_.clear();
    this->test_channels_.clear();
    emit clearDevicesModel();
    emit clearTestingModel();
    emit resultReady(0);
}

bool CANDriverWorker::send_command(PCANMSG cmd_msg)
{
    static uint8_t cmd_counter = 0;

    CHAR errorBuffer[VCI_MAX_ERRSTRLEN];
    HRESULT vciResult;

    cmd_msg->abData[0] = cmd_counter;
    if ( (vciResult = canChannelSendMessage(this->hChannel_, 100, cmd_msg)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        return true;
    }

    return false;
}

bool CANDriverWorker::recv_reply(int32_t timeout, const CANMSG & cmd_msg, CANMSG & reply_msg)
{
    CHAR errorBuffer[VCI_MAX_ERRSTRLEN];
    HRESULT vciResult;

    for ( QTime start_time = QTime::currentTime(); timeout >= 0; timeout -= start_time.msecsTo(QTime::currentTime()) )
    {
        vciResult = canChannelReadMessage(this->hChannel_, timeout, &reply_msg);
        if ( vciResult != VCI_OK )
        {
            vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
            emit errorOccured(errorBuffer);
            return true;
        }
        if ( reply_msg.uMsgInfo.Bits.type != CAN_MSGTYPE_DATA )
        {
            emit warningOccured("Ошибка IXXAT при обработке команды");
            continue;
        }
        if ( reply_msg.dwMsgId != cmd_msg.dwMsgId || reply_msg.uMsgInfo.Bits.dlc < 2
             || (reply_msg.abData[1] & 0x3F) != cmd_msg.abData[1] || reply_msg.abData[0] != cmd_msg.abData[0] )
            continue;
        if ( reply_msg.abData[1] & 0x40 )
        {
            emit errorOccured("Ошибка выполнения команды модулем");
            return true;
        }
        return false;
    }

    emit errorOccured("Таймаут выполнения команды модулем");
    return true;
}

void CANDriverWorker::scan_devices()
{
    CANMSG can_msg = {};
    can_msg.uMsgInfo.Bits.type = CAN_MSGTYPE_DATA;
    can_msg.uMsgInfo.Bits.dlc = 1;

    CHAR errorBuffer[VCI_MAX_ERRSTRLEN];
    HRESULT vciResult;

    if ( (vciResult = canChannelSendMessage(this->hChannel_, 100, &can_msg)) != VCI_OK )
    {
        vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
        emit errorOccured(errorBuffer);
        emit resultReady(1);
        return;
    }

    int32_t timeout = 2000;
    for ( QTime start_time = QTime::currentTime(); timeout >= 0; timeout -= start_time.msecsTo(QTime::currentTime()) )
    {
        vciResult = canChannelReadMessage(this->hChannel_, timeout, &can_msg);
        if ( vciResult == VCI_E_TIMEOUT )
            break;
        if ( vciResult != VCI_OK )
        {
            vciFormatError(vciResult, &errorBuffer[0], sizeof(errorBuffer));
            emit errorOccured(errorBuffer);
            emit resultReady(1);
            return;
        }
        if ( can_msg.uMsgInfo.Bits.type != CAN_MSGTYPE_DATA )
        {
            emit warningOccured("Ошибка IXXAT при поиске модулей");
            continue;
        }
        if ( can_msg.dwMsgId != 0 || can_msg.abData[0] != 0x80 )
        {
            continue;
        }
        if ( can_msg.uMsgInfo.Bits.dlc != 5 )
        {
            emit warningOccured("Неверный размер ответа при поиске модулей");
            continue;
        }
        uint16_t device_id = *reinterpret_cast<uint16_t *>(&can_msg.abData[1]);
        uint16_t device_serial = *reinterpret_cast<uint16_t *>(&can_msg.abData[3]);
        this->devices_[device_id] = device_serial;

#define CMD06_PARSER(type, channel_id, channel_role) channel_test_t({channel_id, std::bind(&CANDriverWorker::cmd06_parser<type>, _1, (channel_id), (channel_role), _2)})
        if ( device_id >= MB_base_device_id )
        {
            test_channels_.emplace(device_id, CMD06_PARSER(uint8_t, 1, ChannelsDataModel::RoleMB));
            test_channels_.emplace(device_id, CMD06_PARSER(uint8_t, 2, ChannelsDataModel::RoleCHARGE));
        }
        else switch (device_id)
        {
        case MP_device_id:
            test_channels_.emplace(device_id, CMD06_PARSER(uint8_t, 1, ChannelsDataModel::RoleMP));
            break;
        case MIG_device_id:
            test_channels_.emplace(device_id, CMD06_PARSER(uint32_t, 1, ChannelsDataModel::RoleMIG));
            test_channels_.emplace(device_id, CMD06_PARSER(float, 2, ChannelsDataModel::RoleDEVI));
            test_channels_.emplace(device_id, CMD06_PARSER(float, 3, ChannelsDataModel::RoleGTF));
            break;
        case NNK_device_id:
            test_channels_.emplace(device_id, CMD06_PARSER(uint8_t, 1, ChannelsDataModel::Role2NNK));
            break;
        case IK_device_id:
            test_channels_.emplace(device_id, CMD06_PARSER(uint8_t, 1, ChannelsDataModel::RoleMIK));
            break;
        default:
            break;
        }
#undef CMD06_PARSER


        emit appendDeviceToModel(device_id, device_serial);
    }

    foreach (auto device, this->devices_)
    {
        CANMSG reply_msg = {};
        CANMSG dev_msg = {};
        dev_msg.uMsgInfo.Bits.type = CAN_MSGTYPE_DATA;
        dev_msg.dwMsgId = device.first;

        if ( device.first == MP_device_id )
        {
            dev_msg.uMsgInfo.Bits.dlc = 3;
            dev_msg.abData[1] = 0x26;
            dev_msg.abData[2] = 1;
        }
        else if ( device.first == MIG_device_id || device.first >= MB_base_device_id )
        {
            dev_msg.uMsgInfo.Bits.dlc = 4;
            dev_msg.abData[1] = 0x20;
            dev_msg.abData[2] = 0;
            dev_msg.abData[3] = 0;
        }
        if ( this->send_command(&dev_msg) || this->recv_reply(2000, dev_msg, reply_msg) )
        {
            emit resultReady(1);
            return;
        }

        if ( device.first >= MB_base_device_id )
        {
            dev_msg.uMsgInfo.Bits.dlc = 7;
            dev_msg.abData[1] = 0x22;
            dev_msg.abData[2] = 0;

            time_t currentTime = std::time(0);
            uint32_t currentUTC = static_cast<uint32_t>(currentTime);
            std::memcpy(&dev_msg.abData[3], &currentUTC, sizeof(currentUTC));
            if ( this->send_command(&dev_msg) || this->recv_reply(2000, dev_msg, reply_msg) )
            {
                emit resultReady(1);
                return;
            }
        }
    }

    emit resultReady(0);
}

void CANDriverWorker::read_calibrations()
{
    foreach (auto device, this->devices_)
    {
        CANMSG dev_msg = {};
        dev_msg.uMsgInfo.Bits.type = CAN_MSGTYPE_DATA;
        dev_msg.dwMsgId = device.first;
        dev_msg.uMsgInfo.Bits.dlc = 8;
        dev_msg.abData[1] = 0x05;

        if ( device.first == MP_device_id )
        {
            cal_MP_t calib;
            uint8_t *calib_ptr = reinterpret_cast<uint8_t *>(&calib);
            uint16_t CRC = 0;
            dev_msg.abData[6] = sizeof(calib);

            if ( this->send_command(&dev_msg) )
            {
                emit resultReady(1);
                return;
            }
            for ( int reply_size = 0; reply_size < sizeof(calib); )
            {
                CANMSG reply_msg = {};
                if ( this->recv_reply(2000, dev_msg, reply_msg) )
                {
                    emit resultReady(1);
                    return;
                }
                reply_size += reply_msg.uMsgInfo.Bits.dlc - 2;
                for ( unsigned int idx = 2; idx < reply_msg.uMsgInfo.Bits.dlc; ++idx, ++calib_ptr )
                {
                    *calib_ptr = reply_msg.abData[idx];
                    if ( calib_ptr < reinterpret_cast<uint8_t *>(&calib.CRC) )
                        CRC += reply_msg.abData[idx];
                }
                ++dev_msg.abData[0];
            }
            CRC = ~CRC;
            break;
        }
        else if ( device.first >= MB_base_device_id )
        {
            cal_MB_t calib;
            uint8_t *calib_ptr = reinterpret_cast<uint8_t *>(&calib);
            uint16_t CRC = 0;
            dev_msg.abData[6] = sizeof(calib);

            if ( this->send_command(&dev_msg) )
            {
                emit resultReady(1);
                return;
            }
            for ( int reply_size = 0; reply_size < sizeof(calib); )
            {
                CANMSG reply_msg = {};
                if ( this->recv_reply(2000, dev_msg, reply_msg) )
                {
                    emit resultReady(1);
                    return;
                }
                reply_size += reply_msg.uMsgInfo.Bits.dlc - 2;
                for ( unsigned int idx = 2; idx < reply_msg.uMsgInfo.Bits.dlc; ++idx, ++calib_ptr )
                {
                    *calib_ptr = reply_msg.abData[idx];
                    if ( calib_ptr < reinterpret_cast<uint8_t *>(&calib.CRC) )
                        CRC += reply_msg.abData[idx];
                }
                ++dev_msg.abData[0];
            }
            CRC = ~CRC;
            ++calib_ptr;
            break;
        }
    }

    emit resultReady(0);
}

void CANDriverWorker::start_testing()
{
    this->timer_id_ = this->startTimer(1000);
    emit resultReady(0);
}

void CANDriverWorker::stop_testing()
{
    killTimer(this->timer_id_);
    emit resultReady(0);
}

void CANDriverWorker::timer_timeout()
{
    if ( this->devices_.empty() )
    {
        emit warningOccured("Нет модулей для тестирования");
        return;
    }
    CANMSG dev_msg = {};
    dev_msg.uMsgInfo.Bits.type = CAN_MSGTYPE_DATA;
    dev_msg.uMsgInfo.Bits.dlc = 3;
    dev_msg.abData[1] = 0x06;
    foreach (auto channel, test_channels_)
    {
        if ( !this->devices_.count(channel.first) )
            continue;
        CANMSG reply_msg = {};
        dev_msg.dwMsgId = channel.first;
        dev_msg.abData[2] = channel.second.id;
        if ( this->send_command(&dev_msg) || this->recv_reply(1000, dev_msg, reply_msg) )
        {
            return;
        }
        else if ( channel.second.parser(this, reply_msg) )
        {
            // TODO: handle error
        }
    }
    this->timer_id_ = this->startTimer(1000);
}

template<typename T>
bool CANDriverWorker::cmd06_parser(const uint8_t channel_id, const int channel_role, const CANMSG & reply_msg)
{
    if ( reply_msg.uMsgInfo.Bits.dlc < 3 + sizeof(T) )
    {
        emit warningOccured("Неверный размер ответа при тестировании модулей");
        return true;
    }
    T value;
    std::memcpy(&value, &reply_msg.abData[3], sizeof(T));
    QString data;
    if ( channel_id == 1 )
    {
        assert(std::is_integral<T>::value);
        assert(sizeof(T) <= sizeof(uint32_t));
        data.setNum(static_cast<uint32_t>(value), 16);
    }
    else
        data.setNum(value);
    emit appendTestDataToModel(channel_role, data);

    return false;
}

void CANDriverWorker::timerEvent(QTimerEvent *event)
{
    killTimer(event->timerId());
    this->timer_timeout();
}
