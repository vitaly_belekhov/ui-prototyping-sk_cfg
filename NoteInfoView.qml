import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import SK.Toolstring 1.0

GridLayout {
    anchors.fill: parent
    anchors.margins: 6
    NoteObject
    {
        id: currentNoteObject
        Component.onCompleted: appCANDriver.noteObject = currentNoteObject;
    }
    columns: 2
    Text {
        Layout.alignment: Qt.AlignLeft
        font.pointSize: 10
        text: qsTr("Исполнитель")
        renderType: Text.NativeRendering
    }
    TextField {
        Layout.alignment: Qt.AlignRight
        Layout.fillWidth: true
        font.pointSize: 10
        text: qsTr("Пневмошланггеофизика")
    }
    Text {
        Layout.alignment: Qt.AlignLeft
        font.pointSize: 10
        text: qsTr("Оператор")
        renderType: Text.NativeRendering
    }
    TextField {
        Layout.alignment: Qt.AlignRight
        Layout.fillWidth: true
        font.pointSize: 10
        text: qsTr("Беднов Андрей")
    }
    Text {
        Layout.alignment: Qt.AlignLeft
        font.pointSize: 10
        text: qsTr("Заказчик")
        renderType: Text.NativeRendering
    }
    TextField {
        Layout.alignment: Qt.AlignRight
        Layout.fillWidth: true
        font.pointSize: 10
        text: qsTr("Нефтегаздобыча")
    }
    Text {
        Layout.alignment: Qt.AlignLeft
        font.pointSize: 10
        text: qsTr("Месторождение")
        renderType: Text.NativeRendering
    }
    TextField {
        Layout.alignment: Qt.AlignRight
        Layout.fillWidth: true
        font.pointSize: 10
        text: qsTr("Североюжное")
    }
    Text {
        Layout.alignment: Qt.AlignLeft
        font.pointSize: 10
        text: qsTr("Куст")
        renderType: Text.NativeRendering
    }
    TextField {
        Layout.alignment: Qt.AlignRight
        Layout.fillWidth: true
        font.pointSize: 10
        text: qsTr("123/Ю")
    }
    Text {
        Layout.alignment: Qt.AlignLeft
        font.pointSize: 10
        text: qsTr("Скважина")
        renderType: Text.NativeRendering
    }
    TextField {
        Layout.alignment: Qt.AlignRight
        Layout.fillWidth: true
        font.pointSize: 10
        text: qsTr("45")
    }

    Item {
        Layout.fillHeight: true
    }
}
