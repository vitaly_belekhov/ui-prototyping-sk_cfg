#include "candriver.h"
#include "candriverworker.h"
#include "noteobject.h"
#include "founddevicesmodel.h"
#include "channelsdatamodel.h"

CANDriver::CANDriver(QObject *parent)
    : QObject(parent)
{
    worker_ = new CANDriverWorker;
    worker_->moveToThread(&workerThread_);
    connect(&workerThread_, &QThread::finished, worker_, &QObject::deleteLater);
    connect(this, &CANDriver::initialize, worker_, &CANDriverWorker::initialize);
    connect(this, &CANDriver::open, worker_, &CANDriverWorker::open);
    connect(this, &CANDriver::reset, worker_, &CANDriverWorker::reset);
    connect(this, &CANDriver::scan_devices, worker_, &CANDriverWorker::scan_devices);
    connect(this, &CANDriver::read_calibrations, worker_, &CANDriverWorker::read_calibrations);
    connect(this, &CANDriver::start_testing, worker_, &CANDriverWorker::start_testing);
    connect(this, &CANDriver::stop_testing, worker_, &CANDriverWorker::stop_testing);
    connect(worker_, &CANDriverWorker::errorOccured, this, &CANDriver::errorOccured);
    connect(worker_, &CANDriverWorker::warningOccured, this, &CANDriver::warningOccured);
    connect(worker_, &CANDriverWorker::resultReady, this, &CANDriver::resultReady);
    workerThread_.start();
}

NoteObject* CANDriver::noteObject() const
{
    return noteObject_;
}

void CANDriver::setNoteObject(NoteObject *noteObject)
{
    if (noteObject_ == noteObject)
        return;

    noteObject_ = noteObject;
    connect(worker_, &CANDriverWorker::clearTestingModel, noteObject_, &NoteObject::clearTestChannelsDataModel);
    connect(worker_, &CANDriverWorker::appendTestDataToModel,
            noteObject_->testChannelsDataModel(), &ChannelsDataModel::append);
    connect(worker_, &CANDriverWorker::clearDevicesModel, noteObject_, &NoteObject::clearFoundDevicesModel);
    connect(worker_, SIGNAL(appendDeviceToModel(const uint16_t, const uint16_t)),
            noteObject_->foundDevicesModel(), SLOT(append(const uint16_t, const uint16_t)));
    emit noteObjectChanged(noteObject);
}
