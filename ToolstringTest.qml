import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: root
    anchors.fill: parent
    anchors.margins: 6

    function initFunction() {
        testTable.state = "TESTING";
    }

    states: [
        State {
            name: "VISIBLE"
            when: (root.visible)
            PropertyChanges
            {
                target: redoAction;
                onTriggeredFunction: root.initFunction
            }
            PropertyChanges { target: redoAction; enabled: true }
        }
    ]

    Component.onCompleted: initFunction()

    TableView {
        Layout.fillWidth: true
        Layout.fillHeight: true
        id: testTable
        model: appCANDriver.noteObject.testChannelsDataModel

        states: [
            State {
                name: "STOPPED"
                when: (!root.visible)
                StateChangeScript {
                    script: { appCANDriver.stop_testing(); }
                }
            },
            State {
                name: "TESTING"
                StateChangeScript {
                    script: { appCANDriver.start_testing(); }
                }
            }
        ]

        Timer {
            id: scrollTimer
            interval: 0
            onTriggered: testTable.positionViewAtRow(testTable.rowCount - 1, ListView.Contain)
        }
        onRowCountChanged: scrollTimer.restart()

        TableViewColumn {
            title: qsTr("МП")
            width: 90
            role: "МП"
            movable: false
        }
        TableViewColumn {
            title: qsTr("МБ")
            width: 90
            role: "МБ"
            movable: false
        }
        TableViewColumn {
            title: qsTr("CHARGE")
            width: 90
            role: "CHARGE"
            movable: false
        }
        TableViewColumn {
            title: qsTr("МИГ")
            width: 90
            role: "МИГ"
            movable: false
        }
        TableViewColumn {
            title: qsTr("DEVI")
            width: 90
            role: "DEVI"
            movable: false
        }
        TableViewColumn {
            title: qsTr("GTF")
            width: 90
            role: "GTF"
            movable: false
        }
        TableViewColumn {
            title: qsTr("2ННК")
            width: 90
            role: "2ННК"
            movable: false
        }
        TableViewColumn {
            title: qsTr("МИК")
            width: 90
            role: "МИК"
            movable: false
        }
    }

    ApplicationLogView {
    }
}

