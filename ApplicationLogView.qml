import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

TableView {
    Layout.fillWidth: true
    model: applicationLogModel
    itemDelegate: Item {
        height: Math.max(16, label.implicitHeight)
        property int implicitWidth: label.implicitWidth + 16

        Text {
            id: label
            objectName: "label"
            width: parent.width
            font.bold: styleData.row >= 0 && model.is_highlevel
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: styleData["depth"] && styleData.column === 0 ? 0 : 8
            horizontalAlignment: styleData.textAlignment
            anchors.verticalCenter: parent.verticalCenter
            elide: styleData.elideMode
            text: styleData.value !== undefined ? styleData.value : ""
            color: styleData.row >= 0 && model.is_error ? "red" : "black"
            renderType: Text.NativeRendering
        }
    }

    TableViewColumn {
        title: qsTr("Время")
        role: "time"
        movable: false
    }
    TableViewColumn {
        title: qsTr("Источник")
        role: "source"
        movable: false
    }
    TableViewColumn {
        title: qsTr("Сообщение")
        role: "message"
        movable: false
        width: -1
    }

    Timer {
        id: scrollTimer
        interval: 0
        onTriggered: positionViewAtRow(rowCount - 1, ListView.Contain)
    }

    onRowCountChanged: scrollTimer.restart()
}
