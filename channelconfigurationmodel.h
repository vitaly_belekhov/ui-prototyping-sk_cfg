#ifndef CHANNELCONFIGURATIONMODEL_H
#define CHANNELCONFIGURATIONMODEL_H

#include <QObject>
#include <QAbstractItemModel>
#include <cstdint>
#include <deque>

typedef struct
{
    uint16_t device_id;
    uint8_t channel_id;
    uint8_t bits;
    bool parity;
    QString name;
} channel_config_t;

typedef std::deque<channel_config_t> channel_config_list_t;
typedef std::deque<channel_config_list_t> mode_config_t;

class ChannelsConfigurationModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum {
        NameRole = Qt::UserRole + 1,
        BitsRole,
        ParityRole
    };

    explicit ChannelsConfigurationModel(QObject *parent = 0);

signals:

public slots:
    Q_INVOKABLE void reset(const mode_config_t & mode_config);

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    virtual QModelIndex parent(const QModelIndex &child) const override;
    Q_INVOKABLE int maxColumnCount() const;

protected:
    mode_config_t mode_config_;
    int column_count_;
};

#endif // CHANNELCONFIGURATIONMODEL_H
