#include "noteobject.h"
#include "founddevicesmodel.h"
#include "channelconfigurationmodel.h"
#include "channelsdatamodel.h"

NoteObject::NoteObject(QObject *parent)
    : QObject(parent)
    , foundDevicesModel_(new FoundDevicesModel)
    , testChannelsDataModel_(new ChannelsDataModel)
    , dynamicChannelsConfigurationModel_(new ChannelsConfigurationModel)
    , staticChannelsConfigurationModel_(new ChannelsConfigurationModel)
{
    this->resetDynamicChannelsConfigurationModel();
    this->resetStaticChannelsConfigurationModel();
}

FoundDevicesModel *NoteObject::foundDevicesModel()
{
    return foundDevicesModel_.get();
}

void NoteObject::clearFoundDevicesModel() const
{
    this->foundDevicesModel_->clear();
}

ChannelsDataModel *NoteObject::testChannelsDataModel()
{
    return testChannelsDataModel_.get();
}

void NoteObject::clearTestChannelsDataModel() const
{
    this->testChannelsDataModel_->clear();
}

ChannelsConfigurationModel *NoteObject::dynamicChannelsConfigurationModel()
{
    return dynamicChannelsConfigurationModel_.get();
}

void NoteObject::resetDynamicChannelsConfigurationModel() const
{
    static mode_config_t mode_config = {
        { {0, 0, 6, true, "TFA"} },
        { {0, 0, 6, true, "TFA"}, {0, 0, 6, true, "VibLv"} },
        { {0, 0, 6, true, "TFA"}, {0, 0, 6, true, "VibLv"}, {0, 0, 8, true, "GR"} },
    };
    this->dynamicChannelsConfigurationModel_->reset(mode_config);
}

ChannelsConfigurationModel *NoteObject::staticChannelsConfigurationModel()
{
    return staticChannelsConfigurationModel_.get();
}

void NoteObject::resetStaticChannelsConfigurationModel() const
{
    static mode_config_t mode_config = {
        { {0, 0, 11, true, "DEVI"}, {0, 0, 12, true, "AZI"}, {0, 0, 8, true, "TEMP"}, {0, 0, 5, false, "CHARGE"}, {0, 0, 3, true, "STAT"} },
        { {0, 0, 11, true, "DEVI"}, {0, 0, 12, true, "AZI"}, {0, 0, 11, true, "DIP"}, {0, 0, 12, true, "HTOT"}, {0, 0, 12, true, "GTOT"}, {0, 0, 8, true, "TEMP"}, {0, 0, 8, false, "BATV"} },
        { {0, 0, 11, true, "AX"}, {0, 0, 11, true, "AY"}, {0, 0, 11, true, "AZ"}, {0, 0, 12, true, "HX"}, {0, 0, 12, true, "HY"}, {0, 0, 12, true, "HZ"}, {0, 0, 8, true, "TEMP"}, {0, 0, 5, true, "CHARGE"} },
    };
    this->staticChannelsConfigurationModel_->reset(mode_config);
}
