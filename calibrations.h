#ifndef CALIBRATIONS
#define CALIBRATIONS

#include <cstdint>

#pragma pack(push, 1)

typedef struct
{
    uint8_t chunkId[4];
    uint32_t chunkSize;

    uint8_t Subchunk1Id[4];
    uint32_t subchunk1Size;
    uint8_t DATE[8];
    uint16_t SERIAL_NUMBER;
    uint8_t CAN_SPEED;
    uint8_t FLASH_MODE;
    uint8_t Subchunk2Id[4];
    uint32_t subchunk2Size;
} cal_header_t;

typedef struct
{
    cal_header_t header;
    uint16_t SPEED;
    uint16_t I_MAX;
    uint16_t T_PWM_INIT;
    uint16_t T_PWM_MAX;
    uint16_t S_STAB;
    int16_t N_SET;
    int16_t G_COR;
    int16_t BIAS;
    int16_t SHOCK_X_OFFSET;
    int16_t SHOCK_X_GAIN;
    int16_t SHOCK_Y_OFFSET;
    int16_t SHOCK_Y_GAIN;
    int16_t SHOCK_Z_OFFSET;
    int16_t SHOCK_Z_GAIN;
    int16_t PRES_OFFSET;
    int16_t PRES_GAIN;
    float Tmin;
    float Tmax;
    float N_MEASmin;
    float N_MEASmax;
    float T_MEASmin;
    float T_MEASmax;
    float I_AVEmin;
    float I_AVEmax;
    float US_PWRmin;
    float US_PWRmax;
    float UD_PWRmin;
    float UD_PWRmax;
    float TmU36min;
    float TmU36max;
    float TmI36min;
    float TmI36max;
    float TmPmin;
    float TmPmax;
    float TmShockmin;
    float TmShockmax;
    uint16_t CRC;
} cal_MP_t;

typedef struct
{
    cal_header_t header;
    uint16_t BAT_C;
    uint16_t LIMIT_U;
    uint16_t LIMIT_C_CRIT;
    uint16_t LIMIT_C_PEACK;
    uint16_t TIME_OFF_PEACK;
    uint16_t LIMIT_C;
    uint16_t TIME_OFF_С;
    uint16_t TIME_ON;
    uint16_t Start_PWM_Persent;
    uint16_t Stop_PWM_Persent;
    uint16_t PWM_Ms_CompareVCC;
    uint16_t PWM_V_CompareVCC;
    uint16_t COUNT_ON;
    float Tmin;
    float Tmax;
    float BatImin;
    float BatImax;
    float BatVmin;
    float BatVmax;
    float BatU3_3min;
    float BatU3_3max;
    float BatI3_3min;
    float BatI3_3max;
    uint16_t CRC;
} cal_MB_t;

#pragma pack(pop)

#endif // CALIBRATIONS

