#ifndef CANDRIVER_H
#define CANDRIVER_H

#include <QObject>
#include <QThread>

class NoteObject;

class CANDriverWorker;

class CANDriver : public QObject
{
    Q_OBJECT
public:
    explicit CANDriver(QObject *parent = 0);

    Q_PROPERTY(NoteObject *noteObject READ noteObject WRITE setNoteObject NOTIFY noteObjectChanged)

    NoteObject* noteObject() const;

signals:
    void initialize();
    void open();
    void reset();
    void scan_devices();
    void read_calibrations();
    void start_testing();
    void stop_testing();

    void errorOccured(const QString & errorText);
    void warningOccured(const QString & errorText);
    void resultReady(int result);

    void noteObjectChanged(NoteObject *noteObject);

public slots:
    void setNoteObject(NoteObject *noteObject);

private:
    NoteObject *noteObject_;
    QThread workerThread_;
    CANDriverWorker *worker_;
};

#endif // CANDRIVER_H
