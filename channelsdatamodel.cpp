#include "channelsdatamodel.h"

ChannelsDataModel::ChannelsDataModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

void ChannelsDataModel::append(const int role, const QString data)
{
    auto max_rows = this->rowCount(QModelIndex());
    bool is_new_row = max_rows == channels_data_[role].size();
    if ( is_new_row )
        beginInsertRows(QModelIndex(), max_rows, max_rows);

    channels_data_[role].push_back(data);

    if ( is_new_row )
        endInsertRows();
    else
        dataChanged(index(static_cast<int>(channels_data_[role].size()) - 1),
                    index(static_cast<int>(channels_data_[role].size()) - 1),
        {role});
}

void ChannelsDataModel::clear()
{
    beginResetModel();
    channels_data_.clear();
    endResetModel();
}

int ChannelsDataModel::rowCount(const QModelIndex &) const
{
    return channels_data_.empty() ? 0 : static_cast<int>(std::max_element(channels_data_.begin(),
                                                      channels_data_.end(),
                                                      [](const std::deque<QString> &a, const std::deque<QString> &b) {
                 return a.size() < b.size();
             })->size());
}

QVariant ChannelsDataModel::data(const QModelIndex &index, int role) const
{
    if ( index.isValid() && index.row() < channels_data_[role].size() )
    {
        return channels_data_[role][index.row()];
    }
    else
        return QVariant();
}

QHash<int, QByteArray> ChannelsDataModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[RoleMP] = "МП";
    roles[RoleMB] = "МБ";
    roles[RoleCHARGE] = "CHARGE";
    roles[RoleMIG] = "МИГ";
    roles[RoleDEVI] = "DEVI";
    roles[RoleGTF] = "GTF";
    roles[Role2NNK] = "2ННК";
    roles[RoleMIK] = "МИК";
    return roles;
}
