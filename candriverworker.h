#ifndef CANDRIVERWORKER_H
#define CANDRIVERWORKER_H

#include <QObject>
#include <windows.h>
#include <cstdint>
#include <functional>
#include "CANtype.h"

Q_DECLARE_METATYPE(uint16_t)

class CANDriverWorker : public QObject
{
    Q_OBJECT
public:
    explicit CANDriverWorker(QObject *parent = 0);

signals:
    void errorOccured(const QString & errorText);
    void warningOccured(const QString & errorText);
    void resultReady(int result);
    void clearDevicesModel();
    void appendDeviceToModel(const uint16_t id, const uint16_t serial);
    void clearTestingModel();
    void appendTestDataToModel(const int role, const QString data);

public slots:
    void initialize();
    void open();
    void reset();
    void scan_devices();
    void read_calibrations();
    void start_testing();
    void stop_testing();
    void timer_timeout();

private:
    class handler_guard
    {
    public:
        typedef HRESULT (*deleter_t)(HANDLE handle);

        explicit handler_guard(deleter_t deleter) throw () : deleter_(deleter), handle_(HANDLE(-1)) {}

        handler_guard & operator=(handler_guard &x) throw ()
        {
            if (&x != this)
            {
                this->deleter_ = x.deleter_;
                this->handle_ = x.handle_;
                x.handle_ = HANDLE(-1);
            }
            return *this;
        }

        ~handler_guard()
        {
            if ( this->handle_ != HANDLE(-1) )
                this->deleter_(this->handle_);
        }

        operator HANDLE() throw() { return this->handle_; }

        operator HANDLE *() { this->reset(); return &this->handle_; }

        void reset() { if ( this->handle_ != HANDLE(-1) ) { this->deleter_(this->handle_); this->handle_ = HANDLE(-1); } }

    private:
        deleter_t deleter_;
        HANDLE handle_;
    };

    typedef std::map<uint16_t, uint16_t> sk_devices_t;

    bool send_command(PCANMSG cmd_msg);
    bool recv_reply(int32_t timeout, const CANMSG & cmd_msg, CANMSG & reply_msg);

    handler_guard hChannel_;
    sk_devices_t devices_;
    int timer_id_;

    template<typename T>
    bool cmd06_parser(const uint8_t channel_id, const int channel_role, const CANMSG & reply_msg);

    typedef std::function<bool (CANDriverWorker *t, const CANMSG & reply_msg)> cmd06_parser_t;
    typedef struct
    {
        uint8_t id;
        cmd06_parser_t parser;
    } channel_test_t;

    std::multimap<uint16_t, channel_test_t> test_channels_;

    // QObject interface
protected:
    virtual void timerEvent(QTimerEvent *) override;
};

#endif // CANDRIVERWORKER_H
